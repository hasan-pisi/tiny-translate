# frozen_string_literal: true

require_relative 'boot'

require 'rails'

Bundler.require(*Rails.groups)

module TinyTranslate
  class Application < Rails::Application
    # Enable locale fallbacks for I18n (makes lookups for any locale fall back to
    # the I18n.default_locale when a translation can not be found).
    # We have to explicitly set default locale since 1.1.0 - see:
    # https://github.com/svenfuchs/i18n/pull/415
    config.i18n.fallbacks = [:en]

    # Configure the default encoding used in templates for Ruby 1.9.
    config.encoding = 'utf-8'
  end
end
