# frozen_string_literal: true

module Gitlab
  module Utils
    extend self

    def random_string
      Random.rand(Float::MAX.to_i).to_s(36)
    end
  end
end
