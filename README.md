# Tiny Translate

A tiny project for testing the GitLab integration with Crowdin.

Includes the linting process, configuration and a simple pipeline from the GitLab project to test out changes to the
translation sync process before changing things in the GitLab repo.
